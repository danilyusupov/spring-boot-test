package com.gdc.apps;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SpringBootTestApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(SpringBootTestApplication.class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }

}
