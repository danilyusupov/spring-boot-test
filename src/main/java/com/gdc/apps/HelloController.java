package com.gdc.apps;

import com.gdc.apps.dao.ProductDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {

    private final ProductDao productDao;

    public HelloController(ProductDao productDao) {
        this.productDao = productDao;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hello")
    public String hello(Model model) {
        model.addAttribute("name", "Данил");
        model.addAttribute("productCount", productDao.getCount());
        return "hello";
    }

}
