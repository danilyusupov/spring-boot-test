package com.gdc.apps.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PostgresProductDao implements ProductDao {

    private final JdbcTemplate jdbcTemplate;

    public PostgresProductDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long getCount() {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM products.items", Long.class);
    }

}
